# polysemy-webserver

A simple wrapper around Warp and Wai to make it easier to write web servers
using Polysemy.

See test/Spec.hs for a simple example of how to use it.
